package com.smf.tutorial.process;

import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;

import com.smf.tutorial.data.EditionCourse;
import com.smf.tutorial.data.EditionStudent;

public class StudentScore extends BaseProcessActionHandler {

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();
    OBContext.setAdminMode(true);

    try {

      result = new JSONObject();
      JSONObject request = new JSONObject(content);
      JSONObject params = request.getJSONObject("_params");

      ///////////////////////////////////////////////////////////////////////////////////////////////// STUDENT
      // Obtain STRING ID entered
      String adStudent = (String) params.getString("c_bpartner_id");

      // Find STUDENT on DB
      OBCriteria<BusinessPartner> businessPartner = OBDal.getInstance()
          .createCriteria(BusinessPartner.class);
      businessPartner.add(Restrictions.eq(BusinessPartner.PROPERTY_ID, adStudent));

      List<BusinessPartner> listBusinessPartner = businessPartner.list();

      // Get STUDENT ID from DB
      BusinessPartner idStudent = listBusinessPartner.get(0); // Student

      ///////////////////////////////////////////////////////////////////////////////////////////////// EDITION-COURSE
      // Obtain EDITION/COURSE ID entered
      String adEditionCourse = params.getString("smft_editioncourse_id");

      // Find EDITION-STUDENT on DB
      OBCriteria<EditionCourse> editionCourse = OBDal.getInstance()
          .createCriteria(EditionCourse.class);
      editionCourse.add(Restrictions.eq(EditionCourse.PROPERTY_ID, adEditionCourse));
      List<EditionCourse> listEditionCourse = editionCourse.list();

      // Get EDITION COURSE ID from DB
      EditionCourse idEditionCourse = listEditionCourse.get(0); // EditionCourse

      ///////////////////////////////////////////////////////////////////////////////////////////////// STUDENT-SCORE
      // Obtain EDITION/COURSE ID entered
      String adStudentScore = params.getString("smft_edit_stud_score"); // Score

      ///////////////////////////////////////////////////////////////////////////////////////////////// EDITION-STUDENT

      // Find RECORD of STUDENT on EDITION-COURSE (Active)
      OBCriteria<EditionStudent> editionStudent = OBDal.getInstance()
          .createCriteria(EditionStudent.class);
      editionStudent.add(Restrictions.eq(EditionStudent.PROPERTY_BUSINESSPARTNER, idStudent));
      editionStudent
          .add(Restrictions.eq(EditionStudent.PROPERTY_SMFTEDITIONCOURSE, idEditionCourse));
      editionStudent.add(Restrictions.eq(EditionStudent.PROPERTY_SMFTEDITSTUDACTIVE, true));

      // List the records
      List<EditionStudent> editionStudentList = editionStudent.list();

      // If exist record or not.
      if (editionStudentList.isEmpty()) {
        // I give the possibility to retry the registration
        result.put("retryExecution", true);

        result.put("message", getErrorMessage());
      } else {

        EditionStudent studenScoreUpdate = editionStudentList.get(0);
        studenScoreUpdate.setSmftEditStudScore(adStudentScore);
        OBDal.getInstance().save(studenScoreUpdate);
        OBDal.getInstance().flush();

        result.put("message", getSuccessMessage());

      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  // Function to return a successfully message
  private JSONObject getSuccessMessage() throws JSONException {
    JSONObject msg = new JSONObject();
    msg.put("severity", "success");
    msg.put("text", "Successfully registered score.");
    return msg;
  }

  // Function to return a failed message
  private JSONObject getErrorMessage() throws JSONException {
    JSONObject msg = new JSONObject();
    msg.put("severity", "error");
    msg.put("text", "The student is not registered in this edition of the course.");
    return msg;
  }

}
