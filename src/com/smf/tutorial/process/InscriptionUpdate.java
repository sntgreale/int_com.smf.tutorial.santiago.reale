package com.smf.tutorial.process;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.smf.tutorial.data.EditionStudent;

public class InscriptionUpdate extends DalBaseProcess {

  @Override
  public void doExecute(ProcessBundle bundle) throws Exception {

    // Necessary to insert in DB
    OBContext.setAdminMode(true);

    // Obtain Date Today
    Date today = new Date();

    // Set TODAy at the beginning of the day
    Calendar todayCalendar = Calendar.getInstance();
    todayCalendar.setTime(today);
    todayCalendar.set(Calendar.HOUR, 0);
    todayCalendar.set(Calendar.MINUTE, 0);
    todayCalendar.set(Calendar.SECOND, 0);
    todayCalendar.set(Calendar.MILLISECOND, 0);
    today = todayCalendar.getTime();

    // I obtain the records dated before today and that are active.

    OBCriteria<EditionStudent> editionStudent = OBDal.getInstance()
        .createCriteria(EditionStudent.class);
    editionStudent.add(Restrictions.lt(EditionStudent.PROPERTY_SMFTEDITSTUDDATEOUT, today));
    editionStudent.add(Restrictions.eq(EditionStudent.PROPERTY_SMFTEDITSTUDACTIVE, true));

    // List the records
    List<EditionStudent> editionStudentList = editionStudent.list();

    // Update records (Active = False)
    for (EditionStudent editStud : editionStudentList) {
      editStud.setSmftEditStudActive(false);

      // Save changes
      OBDal.getInstance().save(editStud);

    }

    OBDal.getInstance().flush();

  }
}
