package com.smf.tutorial.process;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;

import com.smf.tutorial.data.EditionCourse;
import com.smf.tutorial.data.EditionStudent;

public class InscriptionProcess extends BaseProcessActionHandler {

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();
    OBContext.setAdminMode(true);

    try {
      result = new JSONObject();
      JSONObject request = new JSONObject(content);
      JSONObject params = request.getJSONObject("_params");

      ///////////////////////////////////////////////////////////////////////////////////////////////// STUDENT
      // Obtain STRING ID entered
      String adStudent = (String) params.getString("c_bpartner_id");

      // Find STUDENT on DB
      OBCriteria<BusinessPartner> businessPartner = OBDal.getInstance()
          .createCriteria(BusinessPartner.class);
      businessPartner.add(Restrictions.eq(BusinessPartner.PROPERTY_ID, adStudent));
      List<BusinessPartner> listBusinessPartner = businessPartner.list();

      // Get STUDENT ID from DB
      BusinessPartner idStudent = listBusinessPartner.get(0);

      ///////////////////////////////////////////////////////////////////////////////////////////////// EDITION-COURSE
      // Obtain EDITION/COURSE ID entered
      String adEditionCourse = params.getString("smft_editioncourse_id");

      // Find EDITION-STUDENT on DB
      OBCriteria<EditionCourse> editionCourse = OBDal.getInstance()
          .createCriteria(EditionCourse.class);
      editionCourse.add(Restrictions.eq(EditionCourse.PROPERTY_ID, adEditionCourse));
      List<EditionCourse> listEditionCourse = editionCourse.list();

      // Get EDITION COURSE ID from DB
      EditionCourse idEditionCourse = listEditionCourse.get(0);

      ///////////////////////////////////////////////////////////////////////////////////////////////// INSCRIPTION-DATE
      // Obtain INCRIPTION DATE entered
      String adInscriptionString = params.getString("smft_edit_stud_date");

      // Format DATE Entered (String => Date)
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      Date adInscriptionDate = format.parse(adInscriptionString);

      ///////////////////////////////////////////////////////////////////////////////////////////////// OBJECT-DB
      // Create Object
      EditionStudent editionStudent = OBProvider.getInstance().get(EditionStudent.class);
      editionStudent.setNewOBObject(true);
      editionStudent.setBusinessPartner(idStudent);
      editionStudent.setSmftEditioncourse(idEditionCourse);
      editionStudent.setSmftEditStudDate(adInscriptionDate);

      // Save the object
      OBDal.getInstance().save(editionStudent);
      OBDal.getInstance().flush();

      // Set a message. (Successfully registered)
      result.put("message", getSuccessMessage());

      // Return a message. (Successfully registered)
      return result;

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      try {
        // I give the possibility to retry the registration
        result.put("retryExecution", true);

        // Set a message. (Failed message)
        result.put("message", getErrorMessage(e.getMessage()));

      } catch (Exception x) {
        x.printStackTrace();
      }

    }
    // Return a result / message
    return result;

  }

  // Function to return a successfully message
  private JSONObject getSuccessMessage() throws JSONException {
    JSONObject msg = new JSONObject();
    msg.put("severity", "success");
    msg.put("text", "Successfully registered student.");
    return msg;
  }

  // Function to return a failed message
  private JSONObject getErrorMessage(String msgError) throws JSONException {
    JSONObject msg = new JSONObject();
    msg.put("severity", "error");
    msg.put("text", msgError);
    return msg;
  }

}
