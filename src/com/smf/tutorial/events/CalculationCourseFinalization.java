package com.smf.tutorial.events;

import java.util.Calendar;
import java.util.Date;

import javax.enterprise.event.Observes;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.plm.Product;

import com.smf.tutorial.data.EditionCourse;
import com.smf.tutorial.data.EditionStudent;

public class CalculationCourseFinalization extends EntityPersistenceEventObserver {

  private static Entity[] entities = {
      ModelProvider.getInstance().getEntity(EditionStudent.ENTITY_NAME) };

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  // OnSave
  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    // EDITION STUDENT
    Entity editionStudentEntity = ModelProvider.getInstance().getEntity(EditionStudent.ENTITY_NAME);

    // EDITION COURSE

    Property editionCourseProperty = editionStudentEntity
        .getProperty(EditionStudent.PROPERTY_SMFTEDITIONCOURSE);
    EditionCourse editionCourse = (EditionCourse) event.getCurrentState(editionCourseProperty);

    // COURSE

    Product course = (Product) editionCourse.getProduct(); // Course
    String durationCourse = Long.toString(course.getSmftCourseduration()); // Course Duration

    // STUDENT

    Property studentProperty = editionStudentEntity
        .getProperty(EditionStudent.PROPERTY_BUSINESSPARTNER);
    BusinessPartner student = (BusinessPartner) event.getCurrentState(studentProperty);

    // INSCRIPTION

    Property dateInscriptionProperty = editionStudentEntity
        .getProperty(EditionStudent.PROPERTY_SMFTEDITSTUDDATE);
    Date dateInscription = (Date) event.getCurrentState(dateInscriptionProperty);

    // NEW DATE => COURSE FINISH

    Calendar courseFinishCalendar = Calendar.getInstance();
    courseFinishCalendar.setTime(dateInscription);
    courseFinishCalendar.add(Calendar.MONTH, Integer.parseInt(durationCourse));
    Date newDateToFinish = courseFinishCalendar.getTime();

    // CHECK IF EXIST REGISTERED
    boolean exist = checkDuplicates(student, editionCourse);

    // If exist or not a record (Edition / Student) || Save or not
    if (!exist) {
      Property editionStudentProperty = editionStudentEntity
          .getProperty(EditionStudent.PROPERTY_SMFTEDITSTUDDATEOUT);
      event.setCurrentState(editionStudentProperty, newDateToFinish);
    } else {
      throw new OBException(
          "There is currently one registration for Student/Edition. Please wait for the current registration to end.");
    }

  }

  // CheckDuplicates
  private boolean checkDuplicates(BusinessPartner student, EditionCourse editionCourse) {
    OBCriteria<EditionStudent> editStud = OBDal.getInstance().createCriteria(EditionStudent.class);
    editStud.add(Restrictions.eq(EditionStudent.PROPERTY_BUSINESSPARTNER, student));
    editStud.add(Restrictions.eq(EditionStudent.PROPERTY_SMFTEDITIONCOURSE, editionCourse));
    editStud.add(Restrictions.eq(EditionStudent.PROPERTY_ACTIVE, true));

    if (editStud.list().isEmpty()) {
      return false;
    } else {
      return true;
    }
  };
}
